package com.alt.openglandroid.render

data class ResolutionSettings(
    val screenResolution: Resolution,
    val renderResolution: Resolution,
)