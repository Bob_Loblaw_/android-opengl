package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.utils.generateConvolutionMatrix

class Stars : CAPreset(
    name = "Stars",
    convolutionMatrix = generateConvolutionMatrix(
        floatArrayOf(
            .565f, -.716f, .565f,
            -.759f, .627f, -.759f,
            .565f, -.716f, .565f
        )
    ),
    activationFunctionRes = """
        float activation(float x) {
          return abs(x);
        }
    """.trimIndent(),
    startingSetGenType = StartingSetGenType.RANDOM_FLOATS,
)