package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.utils.generateConvolutionMatrix

class Worms : CAPreset(
    name = "Worms",
    convolutionMatrix = generateConvolutionMatrix(
        floatArrayOf(
            .68f, -.9f, .68f,
            -.9f, -.66f, -.9f,
            .68f, -.9f, .68f
        )
    ),
    activationFunctionRes = """
        float activation(float x) {
        return -1./pow(2., (0.6*pow(x, 2.)))+1.;
      }
    """.trimIndent(),
    startingSetGenType = StartingSetGenType.RANDOM_FLOATS,
)