package com.alt.openglandroid.opengl_programs

data class ActivationProgram(
    val location: Int,
    val uSampler: Int
)