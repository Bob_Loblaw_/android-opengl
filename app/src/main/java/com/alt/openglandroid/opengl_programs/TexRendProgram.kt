package com.alt.openglandroid.opengl_programs

data class TexRendProgram(
    val location: Int,
    val uZeroColor: Int,
    val uOneColor: Int,
    val uSampler: Int
)


