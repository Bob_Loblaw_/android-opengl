package com.alt.openglandroid.opengl_programs

data class ConvolutionProgram(
    val location: Int,
    val uMatrix: Int,
    val uSampler: Int
)