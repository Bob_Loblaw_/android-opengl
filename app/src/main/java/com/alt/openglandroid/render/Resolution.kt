package com.alt.openglandroid.render

data class Resolution(val x: Int, val y: Int) {
    companion object Presets {
        val _10x15 = Resolution(
            x = 10,
            y = 15,
        )
        val _100x150 = Resolution(
            x = 100,
            y = 150,
        )
        val _200x300 = Resolution(
            x = 200,
            y = 300,
        )
        val _400x600 = Resolution(
            x = 400,
            y = 600,
        )
        val _800x1200 = Resolution(
            x = 800,
            y = 1200,
        )
        val _1000x1500 = Resolution(
            x = 1000,
            y = 1500,
        )
        val _1600x2400 = Resolution(
            x = 1600,
            y = 2400,
        )
    }

    fun getName() : String {
        return "$x x $y"
    }
}
