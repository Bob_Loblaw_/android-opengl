package com.alt.openglandroid.render

import android.graphics.Point
import android.opengl.GLES30
import android.opengl.GLSurfaceView
import androidx.activity.ComponentActivity
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.alt.openglandroid.R
import com.alt.openglandroid.model.PlaybackSpeed
import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.model.presets.CAPreset
import com.alt.openglandroid.opengl_programs.*
import com.alt.openglandroid.utils.ShaderUtils
import com.alt.openglandroid.utils.getRawStringFromResource
import com.alt.openglandroid.view_models.CurrentPresetViewModel
import com.alt.openglandroid.view_models.PlaybackViewModel
import com.alt.openglandroid.view_models.RenderSettingsViewModel
import kotlinx.coroutines.launch
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.IntBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import kotlin.random.Random

class CARenderer(
    private val surfaceView: GLSurfaceView,
    private val activity: ComponentActivity,
) : GLSurfaceView.Renderer {
    companion object {
        private const val FLOATS_PER_VERTEX : Int = 2
        private const val INTRUSION_SIZE: Int = 5 // todo change this to be included in settings!

        private val rect = floatArrayOf(
            -1.0f,   1.0f, // top left
            -1.0f,  -1.0f, // bottom left
            1.0f,   1.0f, // top right
            // --------------------------------
            1.0f,   1.0f, // top right
            -1.0f,  -1.0f, // bottom left
            1.0f,  -1.0f, // bottom right
        )
    }

    private var currentRenderSettings: RenderSettings
    private var currentPreset: CAPreset
    private var currentPlaybackSpeed: PlaybackSpeed

    init {
        val renderSettingsVM : RenderSettingsViewModel by activity.viewModels()
        val currentPresetVM : CurrentPresetViewModel by activity.viewModels()
        val playbackViewModel : PlaybackViewModel by activity.viewModels()

        currentRenderSettings = renderSettingsVM.uiState.value
        currentPreset = currentPresetVM.uiState.value
        currentPlaybackSpeed = playbackViewModel.uiState.value.playbackSpeed

        activity.lifecycleScope.launch {
            renderSettingsVM.uiState.collect {
                if (it == currentRenderSettings) return@collect

                val current = currentRenderSettings
                currentRenderSettings = it
                if (it.resolution != current.resolution) {
                    surfaceView.apply {
                        queueEvent {
                            disposeGLData()
                            generateGLData()
                            requestRender()
                        }
                    }
                } else {
                    surfaceView.requestRender()
                }
            }
        }
        activity.lifecycleScope.launch {
            currentPresetVM.uiState.collect {
                if (it == currentPreset) return@collect

                currentPreset = it
                surfaceView.apply {
                    queueEvent {
                        disposePrograms()
                        generatePrograms()
                        requestRender()
                    }
                }
            }
        }
        activity.lifecycleScope.launch {
            playbackViewModel.uiState.collect {
                currentPlaybackSpeed = it.playbackSpeed
                playbackSync = 0
            }
        }
    }

    private val vbo = ByteBuffer.allocateDirect(
        rect.size * Float.SIZE_BYTES
    ).run {
        order(ByteOrder.nativeOrder())
        asFloatBuffer().apply {
            rect.map { put(it) }
            position(0)
        }
    }

    private val intrusionBuffer = ByteBuffer.allocateDirect(
        INTRUSION_SIZE * INTRUSION_SIZE * Float.SIZE_BYTES
    ).run {
        order(ByteOrder.nativeOrder())
        asFloatBuffer().apply {
            for (i in 0 until INTRUSION_SIZE * INTRUSION_SIZE) {
                put(1f)
            }
            position(0)
        }
    }

    private var intrusion: Point? = null

    private lateinit var texRendProgram: TexRendProgram
    private lateinit var activationProgram: ActivationProgram
    private lateinit var convolutionProgram: ConvolutionProgram

    private var backFBLocation: Int? = null
    private var tex1Location: Int? = null
    private var tex2Location: Int? = null

    private var playbackSync: Int = 0
    private var drawToScreenMarker = false
    private var stepMarker = false

    private fun getEmptyTextureData() : FloatBuffer {
        val sx = currentRenderSettings.resolution.renderResolution.x
        val sy = currentRenderSettings.resolution.renderResolution.y
        val size = sx * sy

        return ByteBuffer.allocateDirect(size * Float.SIZE_BYTES).run {
            order(ByteOrder.nativeOrder())
            asFloatBuffer().apply {
                when (currentPreset.startingSetGenType) {
                    StartingSetGenType.ALL_ZEROES -> {
                        for (i in 0 until size) {
                            put(0f)
                        }
                    }
                    StartingSetGenType.ALL_ONES -> {
                        for (i in 0 until size) {
                            put(1f)
                        }
                    }
                    StartingSetGenType.RANDOM_BOOLS -> {
                        for (i in 0 until size) {
                            put(if (Random.nextBoolean()) 1f else 0f)
                        }
                    }
                    StartingSetGenType.RANDOM_FLOATS -> {
                        for (i in 0 until size) {
                            put(Random.nextFloat())
                        }
                    }
                }
                position(0)
            }
        }
    }

    fun intrude(screenX: Float, screenY: Float) {
        val sX = currentRenderSettings.resolution.screenResolution.x
        val rX = currentRenderSettings.resolution.renderResolution.x
        val sY = currentRenderSettings.resolution.screenResolution.y
        val rY = currentRenderSettings.resolution.renderResolution.y
        val intrusionX: Int = (screenX / sX * rX).toInt()
        val intrusionY: Int = rY - (screenY / sY * rY).toInt()
        intrusion = Point(intrusionX, intrusionY)
        surfaceView.apply {
            if (!drawToScreenMarker && renderMode == GLSurfaceView.RENDERMODE_WHEN_DIRTY) {
                drawToScreenMarker = true
                requestRender()
            }
        }
    }

    fun doOneStep() {
        stepMarker = true
        surfaceView.requestRender()
    }

    override fun onDrawFrame(arg0: GL10?) {
        glClear()

        if (drawToScreenMarker) {
            drawToScreenMarker = false
            setupDrawToScreen()
            drawToScreen()
        } else {
            setupDrawToTexture()
            cycle()
            setupDrawToScreen()
            drawToScreen()
        }
    }

    private fun cycle() {
        if (stepMarker) {
            stepMarker = false
            convolution()
            activation()
            return
        }

        when (currentPlaybackSpeed) {
            PlaybackSpeed.QUARTER -> {
                throttledCycle(3)
            }
            PlaybackSpeed.HALF -> {
                throttledCycle(1)
            }
            PlaybackSpeed.ONE -> {
                convolution()
                activation()
            }
            PlaybackSpeed.TWO -> {
                convolution()
                activation()
                convolution()
                activation()
            }
            PlaybackSpeed.THREE -> {
                convolution()
                activation()
                convolution()
                activation()
                convolution()
                activation()
            }
            PlaybackSpeed.FOUR -> {
                convolution()
                activation()
                convolution()
                activation()
                convolution()
                activation()
                convolution()
                activation()
            }
        }
    }

    private fun throttledCycle(skip: Int) {
        if (playbackSync == 0) {
            convolution()
            activation()
        }
        if (playbackSync == skip) {
            playbackSync = 0
        } else {
            playbackSync += 1
        }
    }

    private fun glClear() {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT)
        GLES30.glClearColor(0f,0f,0f,0f)
    }

    private fun setupDrawToTexture() {
        GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, backFBLocation!!)
        GLES30.glViewport(
            0,
            0,
            currentRenderSettings.resolution.renderResolution.x,
            currentRenderSettings.resolution.renderResolution.y
        )
    }

    private fun setupDrawToScreen() {
        GLES30.glBindFramebuffer(GLES30.GL_FRAMEBUFFER, 0)
        GLES30.glViewport(
            0,
            0,
            currentRenderSettings.resolution.screenResolution.x,
            currentRenderSettings.resolution.screenResolution.y
        )
        GLES30.glUseProgram(texRendProgram.location)
        GLES30.glUniform1i(texRendProgram.uSampler, 1)
        GLES30.glActiveTexture(GLES30.GL_TEXTURE0 + 1)
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, tex1Location!!) // draw from tex1 to screen

        val zero = currentRenderSettings.zeroColor
        val one = currentRenderSettings.oneColor
        GLES30.glUniform4f(texRendProgram.uZeroColor, zero.red, zero.green, zero.blue, zero.alpha)
        GLES30.glUniform4f(texRendProgram.uOneColor, one.red, one.green, one.blue, one.alpha)
    }

    private fun convolution() {
        GLES30.glFramebufferTexture2D(
            GLES30.GL_FRAMEBUFFER,
            GLES30.GL_COLOR_ATTACHMENT0,
            GLES30.GL_TEXTURE_2D,
            tex2Location!!, //write to tex2
            0
        )
        GLES30.glUseProgram(convolutionProgram.location)
        GLES30.glUniform1i(convolutionProgram.uSampler, 1)
        GLES30.glActiveTexture(GLES30.GL_TEXTURE0 + 1)
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, tex1Location!!) //read from tex1
        GLES30.glUniform1fv(convolutionProgram.uMatrix, 9, currentPreset.convolutionMatrix)
        glDraw()
    }

    private fun activation() {
        GLES30.glFramebufferTexture2D(
            GLES30.GL_FRAMEBUFFER,
            GLES30.GL_COLOR_ATTACHMENT0,
            GLES30.GL_TEXTURE_2D,
            tex1Location!!, //write to tex1
            0
        )
        GLES30.glUseProgram(activationProgram.location)
        GLES30.glUniform1i(activationProgram.uSampler, 1)
        GLES30.glActiveTexture(GLES30.GL_TEXTURE0 + 1)
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, tex2Location!!) //read from tex2
        glDraw()
    }

    private fun drawToScreen() {
        maybeIntrude()
        glDraw()
    }

    private fun maybeIntrude() {
        if (intrusion == null) return

        GLES30.glTexSubImage2D(
            GLES30.GL_TEXTURE_2D,
            0,
            intrusion!!.x - INTRUSION_SIZE / 2,
            intrusion!!.y - INTRUSION_SIZE / 2,
            INTRUSION_SIZE,
            INTRUSION_SIZE,
            GLES30.GL_RED,
            GLES30.GL_FLOAT,
            intrusionBuffer
        )
        intrusion = null
    }

    override fun onSurfaceChanged(arg0: GL10?, width: Int, height: Int) {
        GLES30.glViewport(0, 0, width, height)
    }

    override fun onSurfaceCreated(arg0: GL10?, arg1: EGLConfig?) {
        generateGLData()
    }

    private fun generateGLData() {
        generateBackFramebuffer()
        generateTextures()
        generatePrograms()
    }

    /// Generates back framebuffer
    private fun generateBackFramebuffer() {
        val fb = IntBuffer.allocate(1)
        GLES30.glGenFramebuffers(1, fb)
        backFBLocation = fb[0]
    }

    /// Generates two textures
    private fun generateTextures() {
        val textures = IntBuffer.allocate(2)
        GLES30.glGenTextures(2, textures)
        tex1Location = textures[0]
        tex2Location = textures[1]
        fillTexturesWithInitData()
    }

    fun restartSimulation() {
        surfaceView.apply {
            queueEvent {
                fillTexturesWithInitData()
                requestRender()
            }
        }
    }

    private fun fillTexturesWithInitData() {
        val data = getEmptyTextureData()
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, tex1Location!!)
        GLES30.glTexImage2D(
            GLES30.GL_TEXTURE_2D,
            0,
            GLES30.GL_R32F,
            currentRenderSettings.resolution.renderResolution.x,
            currentRenderSettings.resolution.renderResolution.y,
            0,
            GLES30.GL_RED,
            GLES30.GL_FLOAT,
            data
        )
        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_NEAREST)
        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_NEAREST)

        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, tex2Location!!)
        GLES30.glTexImage2D(
            GLES30.GL_TEXTURE_2D,
            0,
            GLES30.GL_R32F,
            currentRenderSettings.resolution.renderResolution.x,
            currentRenderSettings.resolution.renderResolution.y,
            0,
            GLES30.GL_RED,
            GLES30.GL_FLOAT,
            data
        )
        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_NEAREST)
        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_NEAREST)
    }

    /// Creates defined programs
    private fun generatePrograms() {
        val vertexShader = ShaderUtils.compileShader(
            GLES30.GL_VERTEX_SHADER,
            getRawStringFromResource(
                context = activity,
                resId = R.raw.vert
            )
        )

        val activationProgram = ShaderUtils.compileProgram(
            vertexShader,
            ShaderUtils.compileShader(
                GLES30.GL_FRAGMENT_SHADER,
                activity.getString(
                    R.string.activation_shader_template,
                    currentPreset.activationFunctionRes,
                )
            )
        )
        val convolutionProgram = ShaderUtils.compileProgram(
            vertexShader,
            ShaderUtils.compileShader(
                GLES30.GL_FRAGMENT_SHADER,
                getRawStringFromResource(
                    context = activity,
                    resId = R.raw.conv_frag
                )
            )
        )
        val texRendProgram = ShaderUtils.compileProgram(
            vertexShader,
            ShaderUtils.compileShader(
                GLES30.GL_FRAGMENT_SHADER,
                getRawStringFromResource(
                    context = activity,
                    resId = R.raw.tex_rend_frag
                )
            )
        )

        // setup activation program
        GLES30.glUseProgram(activationProgram)
        val uActSampler = GLES30.glGetUniformLocation(activationProgram, "act_source")
        this.activationProgram = ActivationProgram(
            location = activationProgram,
            uSampler = uActSampler,
        )

        // setup convolutionProgram program
        GLES30.glUseProgram(convolutionProgram)
        val uConvSampler = GLES30.glGetUniformLocation(convolutionProgram, "conv_source")
        val uMatrix = GLES30.glGetUniformLocation(convolutionProgram, "u_convolution_matrix")
        this.convolutionProgram = ConvolutionProgram(
            location = convolutionProgram,
            uMatrix = uMatrix,
            uSampler = uConvSampler,
        )

        // setup texture render program
        GLES30.glUseProgram(texRendProgram)
        val u0 = GLES30.glGetUniformLocation(texRendProgram, "u_zero_color")
        val u1 = GLES30.glGetUniformLocation(texRendProgram, "u_one_color")
        val uSampler = GLES30.glGetUniformLocation(texRendProgram, "activated_values")
        this.texRendProgram = TexRendProgram(
            location = texRendProgram,
            uZeroColor = u0,
            uOneColor = u1,
            uSampler = uSampler,
        )
    }

    /// Abstracts low-level gl draw calls
    private fun glDraw() {
        GLES30.glVertexAttribPointer(
            0,
            2,
            GLES30.GL_FLOAT,
            false,
            2 * Float.SIZE_BYTES,
            vbo,
        )
        GLES30.glEnableVertexAttribArray(0)
        GLES30.glDrawArrays(GLES30.GL_TRIANGLES, 0, rect.size / FLOATS_PER_VERTEX )
    }

    private fun disposeGLData() {
        disposeTextures()
        disposePrograms()
    }

    private fun disposeTextures() {
        GLES30.glDeleteTextures(
            2,
            IntBuffer.allocate(2).apply {
                put(tex1Location!!)
                put(tex2Location!!)
                position(0)
            }
        )
    }

    private fun disposePrograms() {
        GLES30.glDeleteProgram(activationProgram.location)
        GLES30.glDeleteProgram(convolutionProgram.location)
        GLES30.glDeleteProgram(texRendProgram.location)
        GLES30.glDeleteFramebuffers(
            1,
            IntBuffer.allocate(1).apply {
                put(backFBLocation!!)
                position(0)
            }
        )
    }
}