#version 300 es

precision highp float;
precision highp sampler2D;

in vec2 f_position;
out float out_value;

uniform float u_convolution_matrix[9];
uniform sampler2D conv_source;

void main() {

    /// this has been tested. Works. Rectangles are where they should be
    /*if (
        f_position.x > .95 && f_position.y > .95 || // top right
        f_position.x > .95 && f_position.y < .05 || // bottom right
        f_position.x < .05 && f_position.y > .95 || // top left
        f_position.x < .05 && f_position.y < .05    // bottom left
    ) {
        out_value = 1.0;
        return;
    }*/

    ivec2 ts = textureSize(conv_source, 0);
    vec2 texel = vec2(
        1.0 / float(ts.x),
        1.0 / float(ts.y)
    );

    vec2 tl = vec2(
        f_position.x - texel.x,
        f_position.y + texel.y
    );
    vec2 tc = vec2(
        f_position.x,
        f_position.y + texel.y
    );
    vec2 tr = vec2(
        f_position.x + texel.x,
        f_position.y + texel.y
    );
    vec2 cl = vec2(
        f_position.x - texel.x,
        f_position.y
    );
    vec2 cc = vec2(
        f_position.x,
        f_position.y
    );
    vec2 cr = vec2(
        f_position.x + texel.x,
        f_position.y
    );
    vec2 bl = vec2(
        f_position.x - texel.x,
        f_position.y - texel.y
    );
    vec2 bc = vec2(
        f_position.x,
        f_position.y - texel.y
    );
    vec2 br = vec2(
        f_position.x + texel.x,
        f_position.y - texel.y
    );

    float conv_res =
        u_convolution_matrix[0] * float(texture(conv_source, tl)) +
        u_convolution_matrix[1] * float(texture(conv_source, tc)) +
        u_convolution_matrix[2] * float(texture(conv_source, tr)) +
        u_convolution_matrix[3] * float(texture(conv_source, cl)) +
        u_convolution_matrix[4] * float(texture(conv_source, cc)) +
        u_convolution_matrix[5] * float(texture(conv_source, cr)) +
        u_convolution_matrix[6] * float(texture(conv_source, bl)) +
        u_convolution_matrix[7] * float(texture(conv_source, bc)) +
        u_convolution_matrix[8] * float(texture(conv_source, br));

    out_value = conv_res;
}
