package com.alt.openglandroid.model

data class Playback(val playbackSpeed: PlaybackSpeed, val isPaused: Boolean)