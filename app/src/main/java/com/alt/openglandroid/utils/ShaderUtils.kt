package com.alt.openglandroid.utils

import android.opengl.GLES30
import android.util.Log

class ShaderUtils {
    companion object {
        fun compileShader(shaderType: Int, shaderSource: String) : Int {
            val shaderInt = GLES30.glCreateShader(shaderType)
            GLES30.glShaderSource(shaderInt, shaderSource)
            GLES30.glCompileShader(shaderInt)

            val compiled = IntArray(1)
            GLES30.glGetShaderiv(shaderInt, GLES30.GL_COMPILE_STATUS, compiled, 0)
            if (compiled[0] == 0) {
                // error compiling
                Log.d(
                    "OpenGL Shader Compilation Error: ",
                    GLES30.glGetShaderInfoLog(shaderInt)
                )
                throw Exception(GLES30.glGetShaderInfoLog(shaderInt))
            }

            return shaderInt
        }

        fun compileProgram(vShader: Int, fShader: Int) : Int {
            val programId = GLES30.glCreateProgram()
            GLES30.glAttachShader(programId, vShader)
            GLES30.glAttachShader(programId, fShader)
            GLES30.glLinkProgram(programId)

            val linked = IntArray(1)
            GLES30.glGetProgramiv(programId, GLES30.GL_LINK_STATUS, linked, 0)
            if (linked[0] == 0) {
                // error compiling
                Log.d(
                    "OpenGL Program Compilation Error: ",
                    GLES30.glGetShaderInfoLog(programId)
                )
                throw Exception(GLES30.glGetShaderInfoLog(programId))
            }

            return programId
        }
    }
}