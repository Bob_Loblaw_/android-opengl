package com.alt.openglandroid.model

import android.graphics.Color

data class OpenGLColor(val red : Float, val green : Float, val blue : Float, val alpha : Float) {

    companion object {
        fun fromARGB(argb: IntArray): OpenGLColor {
            return OpenGLColor(
                red = argb[1] / 256.0f,
                green = argb[2] / 256.0f,
                blue = argb[3] / 256.0f,
                alpha = argb[0] / 256.0f,
            )
        }
    }

    init {
        assert(
            red in 0.0f..1.0f &&
            green in 0.0f..1.0f &&
            blue in 0.0f..1.0f &&
            alpha in 0.0f..1.0f
        )
    }

    fun toAndroidColor() : Color {
        return Color.valueOf(red, green, blue, alpha)
    }
}


