package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.utils.generateConvolutionMatrix

class ConwaysGameOfLife : CAPreset(
    name = "Conway's game of life",
    convolutionMatrix = generateConvolutionMatrix(
        floatArrayOf(
            1f, 1f, 1f,
            1f, -9f, 1f,
            1f, 1f, 1f
        )
    ),
    activationFunctionRes = """
        float activation(float _input) {
            // Any live cell with two or three live neighbours survives.
            // Any dead cell with three live neighbours becomes a live cell.
            // All other live cells die in the next generation. Similarly, all other dead cells stay dead.
            
            if (_input == -7. || _input == -6. || _input == 3.) {
                return 1.0;
            }
            return .0;
        }
    """.trimIndent(),
    startingSetGenType = StartingSetGenType.RANDOM_BOOLS,
)