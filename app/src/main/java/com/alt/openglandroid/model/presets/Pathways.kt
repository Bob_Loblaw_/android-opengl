package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.utils.generateConvolutionMatrix

class Pathways : CAPreset(
    name = "Pathways",
    convolutionMatrix = generateConvolutionMatrix(
        floatArrayOf(
            0f, 1f, 0f,
            1f, 1f, 1f,
            0f, 1f, 0f
        )
    ),
    activationFunctionRes = """
        float gaussian(float x, float b) {
            return 1./pow(2., (pow(x-b, 2.)));
        }

        float activation(float x) {
            return gaussian(x, 3.5);
        }		
    """.trimIndent(),
    startingSetGenType = StartingSetGenType.RANDOM_FLOATS,
)