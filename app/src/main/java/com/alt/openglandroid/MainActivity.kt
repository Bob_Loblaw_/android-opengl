package com.alt.openglandroid

import android.app.ActivityManager
import android.content.Context
import android.content.pm.ConfigurationInfo
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Space
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.doOnLayout
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.alt.openglandroid.databinding.ActivityMainBinding
import com.alt.openglandroid.render.CARenderer
import com.alt.openglandroid.render.Resolution
import com.alt.openglandroid.view_models.PlaybackViewModel
import com.alt.openglandroid.view_models.RenderSettingsViewModel
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var glRenderer: CARenderer
    private var didSetRenderer: Boolean = false

    /*private val scaleListener = object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {

            return super.onScale(detector)
        }
    }
    private val scaleDetector = ScaleGestureDetector(this, scaleListener)*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!supportES3()) {
            Toast.makeText(this, "OpenGl ES 3.0 is not supported", Toast.LENGTH_LONG).show()
            finish()
            return
        }

        WindowCompat.setDecorFitsSystemWindows(window, false)

        val playbackVM : PlaybackViewModel by viewModels()

        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        mainBinding.mainScreen.glSurfaceView.apply {
            setEGLContextClientVersion(3)
            preserveEGLContextOnPause = true
            doOnLayout {
                // instantiate RenderSettingsViewModel with factory
                ViewModelProvider(
                    this@MainActivity,
                    RenderSettingsViewModel.RenderSettingsVMFactory(
                        initialScreenResolution = Resolution(x = width, y = height),
                        initialRenderResolution = Resolution._800x1200,
                    )
                )[RenderSettingsViewModel::class.java]
                glRenderer = CARenderer(
                    activity = this@MainActivity,
                    surfaceView = mainBinding.mainScreen.glSurfaceView,
                )
                setOnTouchListener { _, event ->
                    performClick()
                    //scaleDetector.onTouchEvent(event)
                    glRenderer.intrude(event.x, event.y)
                    true
                }
                setRenderer(glRenderer)
                onResume()
                didSetRenderer = true
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(mainBinding.mainScreen.root) { _, windowInsets ->
            val insets = windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            mainBinding.mainScreen.root.addView(Space(this@MainActivity).apply {
                layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    insets.bottom,
                )
            })
            WindowInsetsCompat.CONSUMED
        }

        mainBinding.mainScreen.actionBar.playPause.setOnClickListener {
            playbackVM.togglePlayPause()
        }

        mainBinding.mainScreen.actionBar.step.setOnClickListener {
            glRenderer.doOneStep()
        }

        mainBinding.mainScreen.actionBar.restart.setOnClickListener {
            glRenderer.restartSimulation()
        }

        mainBinding.mainScreen.actionBar.settings.setOnClickListener {
            //glRenderer.changeRenderSettings(RenderSettingsCycler.cycleAndGet())
            val mbs = BottomSheet()
            mbs.show(supportFragmentManager, BottomSheet.TAG)
        }

        setContentView(mainBinding.root)

        mainBinding.root.doOnLayout {
            playbackVM.viewModelScope.launch {
                playbackVM.uiState.collect {
                    if (it.isPaused) {
                        mainBinding.mainScreen.apply {
                            glSurfaceView.renderMode = GLSurfaceView.RENDERMODE_WHEN_DIRTY
                            actionBar.playPause.setImageResource(R.drawable.ic_play)
                            actionBar.step.visibility = View.VISIBLE
                            actionBar.restart.visibility = View.VISIBLE
                        }
                    } else {
                        mainBinding.mainScreen.apply {
                            glSurfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY
                            actionBar.playPause.setImageResource(R.drawable.ic_pause)
                            actionBar.step.visibility = View.GONE
                            actionBar.restart.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private fun supportES3(): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val configurationInfo: ConfigurationInfo = activityManager.deviceConfigurationInfo
        return configurationInfo.reqGlEsVersion >= 0x30000
    }

    override fun onPause() {
        super.onPause()
        if (didSetRenderer) {
            mainBinding.mainScreen.glSurfaceView.onPause()
        }
    }

    override fun onResume() {
        super.onResume()
        if (didSetRenderer) {
            mainBinding.mainScreen.glSurfaceView.onResume()
        }
    }
}
