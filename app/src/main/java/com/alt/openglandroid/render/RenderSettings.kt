package com.alt.openglandroid.render

import com.alt.openglandroid.model.OpenGLColor

data class RenderSettings(
    val zeroColor: OpenGLColor,
    val oneColor: OpenGLColor,
    val resolution: ResolutionSettings,
)