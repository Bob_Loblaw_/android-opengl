#version 300 es

precision highp float;
precision highp sampler2D;

in vec2 f_position;
out vec4 out_color;

uniform vec4 u_zero_color;
uniform vec4 u_one_color;

uniform sampler2D activated_values;

vec4 lerpv4(vec4 a, vec4 b, float t) {
    return a + (b - a) * t;
}

void main() {
    float sampled = float(texture(activated_values, f_position));
    out_color = lerpv4(u_zero_color, u_one_color, sampled);

    /// below is tested! Works!!!
    /*if (f_position.x > .95 && f_position.y > .95) {
        // top right
        out_color = vec4(1.0, 0.0, 0.0, 1.0);
    } else if (f_position.x > .95 && f_position.y < .05) {
        // bottom right
        out_color = vec4(1.0, 0.0, 0.0, 1.0);
    } else if (f_position.x < .05 && f_position.y > .95) {
        // top left
        out_color = vec4(1.0, 0.0, 0.0, 1.0);
    } else if (f_position.x < .05 && f_position.y < .05) {
        // bottom left
        out_color = vec4(1.0, 0.0, 0.0, 1.0);
    } else {
        out_color = vec4(0.0,0.0,0.0,0.0);
    }*/
}
