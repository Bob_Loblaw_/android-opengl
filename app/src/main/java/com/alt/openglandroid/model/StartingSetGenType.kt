package com.alt.openglandroid.model

enum class StartingSetGenType {
    ALL_ZEROES,
    ALL_ONES,
    RANDOM_FLOATS,
    RANDOM_BOOLS,
}
