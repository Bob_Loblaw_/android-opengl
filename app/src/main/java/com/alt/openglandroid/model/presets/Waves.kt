package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.utils.generateConvolutionMatrix

class Waves : CAPreset(
    name = "Waves",
    convolutionMatrix = generateConvolutionMatrix(
        floatArrayOf(
            .565f, -.716f, .565f,
            -.716f, .627f, -.716f,
            .565f, -.716f, .565f
        )
    ),
    activationFunctionRes = """
        float activation(float x) {
          return abs(1.2*x);
        }
    """.trimIndent(),
    startingSetGenType = StartingSetGenType.ALL_ZEROES,
)