package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import com.alt.openglandroid.utils.generateConvolutionMatrix

class Mitosis : CAPreset(
    name = "Mitosis",
    convolutionMatrix = generateConvolutionMatrix(
        floatArrayOf(
            -.939f, .88f, -.939f,
              .88f,  .4f,  .88f,
            -.939f, .88f, -.939f
        )
    ),
    activationFunctionRes = """
        float activation(float x) {
            return -1./(0.9*pow(x, 2.)+1.)+1.;
        }		
    """.trimIndent(),
    startingSetGenType = StartingSetGenType.RANDOM_FLOATS,
)