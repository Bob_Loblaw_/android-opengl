package com.alt.openglandroid.model

enum class PlaybackSpeed {
    QUARTER,
    HALF,
    ONE,
    TWO,
    THREE,
    FOUR
}