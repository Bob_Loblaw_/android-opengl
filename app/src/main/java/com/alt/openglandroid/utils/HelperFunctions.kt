package com.alt.openglandroid.utils

import android.content.Context
import android.content.res.Resources
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

fun generateConvolutionMatrix(floatArray: FloatArray) : FloatBuffer {
    assert(floatArray.size == 9)
    return ByteBuffer.allocateDirect(9 * Float.SIZE_BYTES).run {
        order(ByteOrder.nativeOrder())
        asFloatBuffer().apply {
            for (i in floatArray) {
                put(i)
            }
            position(0)
        }
    }
}

fun getRawStringFromResource(context: Context, resId: Int) : String {
    return context.resources.openRawResource(resId).bufferedReader().use { it.readText() }
}


