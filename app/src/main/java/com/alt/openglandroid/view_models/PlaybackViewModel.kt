package com.alt.openglandroid.view_models

import androidx.lifecycle.ViewModel
import com.alt.openglandroid.model.Playback
import com.alt.openglandroid.model.PlaybackSpeed
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class PlaybackViewModel() : ViewModel() {
    private val _uiState : MutableStateFlow<Playback> = MutableStateFlow(Playback(
        playbackSpeed = PlaybackSpeed.ONE,
        isPaused = false,
    ))

    val uiState: StateFlow<Playback> = _uiState.asStateFlow()

    fun togglePlayPause() {
        _uiState.update {
            it.copy(
                isPaused = !it.isPaused,
            )
        }
    }

    fun setPlaybackSpeed(playbackSpeed: PlaybackSpeed) {
        _uiState.update {
            it.copy(
                playbackSpeed = playbackSpeed,
            )
        }
    }
}