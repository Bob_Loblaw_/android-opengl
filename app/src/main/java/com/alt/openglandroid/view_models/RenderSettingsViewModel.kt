package com.alt.openglandroid.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.alt.openglandroid.model.OpenGLColor
import com.alt.openglandroid.render.RenderSettings
import com.alt.openglandroid.render.Resolution
import com.alt.openglandroid.render.ResolutionSettings
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class RenderSettingsViewModel(
    initialScreenResolution: Resolution,
    initialRenderResolution: Resolution,
) : ViewModel() {

    class RenderSettingsVMFactory(
        private val initialScreenResolution: Resolution,
        private val initialRenderResolution: Resolution,
    ) :
        ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return RenderSettingsViewModel(
                initialScreenResolution = initialScreenResolution,
                initialRenderResolution = initialRenderResolution,
            ) as T
        }
    }

    private val _uiState : MutableStateFlow<RenderSettings> = MutableStateFlow(RenderSettings(
        zeroColor = OpenGLColor(
            red = 0.0f,
            green = 0.0f,
            blue = 0.0f,
            alpha = 1.0f
        ),
        oneColor = OpenGLColor(
            red = 1.0f,
            green = 1.0f,
            blue = 1.0f,
            alpha = 1.0f
        ),
        resolution = ResolutionSettings(
            screenResolution = initialScreenResolution,
            renderResolution = initialRenderResolution,
        )
    ))

    val uiState: StateFlow<RenderSettings> = _uiState.asStateFlow()

    fun applySettings(settings: RenderSettings) {
        _uiState.update {
            settings
        }
    }
}