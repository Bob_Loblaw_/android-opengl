package com.alt.openglandroid

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.alt.openglandroid.databinding.BottomSheetBinding
import com.alt.openglandroid.model.OpenGLColor
import com.alt.openglandroid.model.PlaybackSpeed
import com.alt.openglandroid.model.presets.*
import com.alt.openglandroid.render.Resolution
import com.alt.openglandroid.view_models.CurrentPresetViewModel
import com.alt.openglandroid.view_models.PlaybackViewModel
import com.alt.openglandroid.view_models.RenderSettingsViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.skydoves.colorpickerview.ColorEnvelope
import com.skydoves.colorpickerview.ColorPickerDialog
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener
import kotlinx.coroutines.launch

class BottomSheet : BottomSheetDialogFragment() {
    companion object {
        const val TAG: String = "Bottom Sheet"
    }

    class ResolutionPickerDialog : DialogFragment() {
        companion object {
            const val TAG : String = "ResolutionPickerDialog"
        }
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val renderSettingsVM : RenderSettingsViewModel by requireActivity().viewModels()

            val presets = arrayOf(
                Resolution._10x15,
                Resolution._100x150,
                Resolution._200x300,
                Resolution._400x600,
                Resolution._800x1200,
                Resolution._1000x1500,
                Resolution._1600x2400,
            )

            return AlertDialog.Builder(requireActivity()).run {
                setTitle(requireActivity().getString(R.string.pick_resolution))
                setItems(presets.map { e -> e .getName() }.toTypedArray()) { _, which ->
                    val v = renderSettingsVM.uiState.value
                    renderSettingsVM.applySettings(v.copy(
                        resolution = v.resolution.copy(
                            renderResolution = presets[which],
                        )
                    ))
                }
                create()
            }
        }
    }

    class PresetPickerDialog : DialogFragment() {
        companion object {
            const val TAG : String = "PresetPickerDialog"
        }
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val presetVM : CurrentPresetViewModel by requireActivity().viewModels()

            val presets = arrayOf(
                ConwaysGameOfLife(),
                Mitosis(),
                Pathways(),
                Stars(),
                Waves(),
                Worms(),
            )

            return AlertDialog.Builder(requireActivity()).run {
                setTitle(requireActivity().getString(R.string.pick_preset))
                setItems(presets.map { e -> e .name }.toTypedArray()) { _, which ->
                    presetVM.changePreset(presets[which])
                }
                create()
            }
        }
    }

    private lateinit var bsBinding: BottomSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val renderSettingsVM : RenderSettingsViewModel by requireActivity().viewModels()
        val playbackVM : PlaybackViewModel by requireActivity().viewModels()
        val currentPresetViewModel: CurrentPresetViewModel by requireActivity().viewModels()

        bsBinding = BottomSheetBinding.inflate(inflater, container, false)
        bsBinding.playbackSlider.value = playbackVM.uiState.value.playbackSpeed.ordinal.toFloat()

        lifecycleScope.launch {
            renderSettingsVM.uiState.collect {
                bsBinding.zeroColorBox.background = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.rounded_colored_box
                ).run {
                    DrawableCompat.wrap(this!!).apply {
                        setTint(it.zeroColor.toAndroidColor().toArgb())
                    }
                }

                bsBinding.oneColorBox.background = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.rounded_colored_box
                ).run {
                    DrawableCompat.wrap(this!!).apply {
                        setTint(it.oneColor.toAndroidColor().toArgb())
                    }
                }

                bsBinding.resolutionText.text = requireActivity().getString(
                    R.string.resolution_display_template,
                    it.resolution.renderResolution.x,
                    it.resolution.renderResolution.y
                )
            }
        }

        lifecycleScope.launch {
            currentPresetViewModel.uiState.collect {
                bsBinding.presetText.text = it.name
            }
        }

        bsBinding.playbackSlider.addOnChangeListener { _, value, _ ->
            val vals = PlaybackSpeed.values()
            playbackVM.setPlaybackSpeed(vals[value.toInt()])
        }

        bsBinding.zeroColorPicker.setOnClickListener {
            colorPicker(requireActivity().getString(R.string.zero_color_text)) {
                renderSettingsVM.applySettings(renderSettingsVM.uiState.value.copy(
                    zeroColor = OpenGLColor.fromARGB(it.argb)
                ))
            }
        }

        bsBinding.oneColorPicker.setOnClickListener {
            colorPicker(requireActivity().getString(R.string.one_color_text)) {
                renderSettingsVM.applySettings(renderSettingsVM.uiState.value.copy(
                    oneColor = OpenGLColor.fromARGB(it.argb)
                ))
            }
        }

        bsBinding.resolutionArea.setOnClickListener {
            ResolutionPickerDialog().show(parentFragmentManager, ResolutionPickerDialog.TAG)
        }

        bsBinding.presetArea.setOnClickListener {
            PresetPickerDialog().show(parentFragmentManager, PresetPickerDialog.TAG)
        }

        return bsBinding.root
    }

    private fun colorPicker(title: String, onOk: (envelope: ColorEnvelope) -> Unit) {
        ColorPickerDialog.Builder(requireContext())
            .setTitle(title)
            .setPreferenceName("ColorPickerDialog")
            .setPositiveButton(
                getString(R.string.confirm),
                ColorEnvelopeListener { envelope, _ ->
                    onOk(envelope)
                }
            )
            .setNegativeButton(getString(R.string.cancel)) {
                dialogInterface, _ -> dialogInterface.dismiss()
            }
            .attachAlphaSlideBar(true)
            .attachBrightnessSlideBar(true)
            .setBottomSpace(12)
            .show()
    }
}
