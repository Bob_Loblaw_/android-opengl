package com.alt.openglandroid.model.presets

import com.alt.openglandroid.model.StartingSetGenType
import java.nio.FloatBuffer

abstract class CAPreset(
    val name: String,
    val convolutionMatrix : FloatBuffer,
    val activationFunctionRes : String,
    val startingSetGenType: StartingSetGenType,
)
