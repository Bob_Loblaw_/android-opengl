package com.alt.openglandroid.view_models

import androidx.lifecycle.ViewModel
import com.alt.openglandroid.model.presets.CAPreset
import com.alt.openglandroid.model.presets.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class CurrentPresetViewModel : ViewModel() {
    private val _uiState : MutableStateFlow<CAPreset> = MutableStateFlow(Waves())
    val uiState: StateFlow<CAPreset> = _uiState.asStateFlow()

    fun changePreset(preset: CAPreset) {
        _uiState.update {
            preset
        }
    }
}